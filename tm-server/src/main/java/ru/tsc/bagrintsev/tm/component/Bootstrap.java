package ru.tsc.bagrintsev.tm.component;

import jakarta.xml.ws.Endpoint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.*;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.*;
import ru.tsc.bagrintsev.tm.endpoint.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.service.*;
import ru.tsc.bagrintsev.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();


    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(domainEndpoint);
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = String.format("http://%s:%s/%s?wsdl", host, port, name);
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void run() {
        initPID();
        initUsers();
        initDemoData();
        loggerService.info("*** Task Manager Server Started ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
        backup.start();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initUsers() {
        try {
            userService.create("test", "test").setEmail("test@test.ru");
            userService.create("admin", "admin").setRole(Role.ADMIN);
        } catch (GeneralSecurityException | AbstractException e) {
            System.err.println("User initialization error...");
            loggerService.error(e);
        }
    }

    private void initDemoData() {
        userRepository.findAll()
                .forEach(user -> {
                    try {
                        initUserData(user.getLogin());
                    } catch (AbstractException e) {
                        System.err.println("Data initialization error...");
                        loggerService.error(e);
                    }
                });
    }

    private void shutdown() {
        loggerService.info("*** Task Manager Server Stopped ***");
        backup.stop();
    }

    private void initUserData(String login) throws AbstractException {
        @NotNull final String userId = userRepository.findByLogin(login).getId();
        taskService.create(userId, String.format("%s first task", login), String.format("%s simple description", login));
        taskService.create(userId, String.format("%s second task", login), String.format("%s simple description", login));
        taskService.create(userId, String.format("%s third task", login), String.format("%s simple description", login));
        taskService.create(userId, String.format("%s fourth task", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s first project", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s second project", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s third project", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s fourth project", login), String.format("%s simple description", login));
    }

}

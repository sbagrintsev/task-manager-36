package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IAbstractRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IAbstractService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectIndexException;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@Nullable final M record) throws AbstractException {
        check(Entity.ABSTRACT, record);
        return repository.add(record);
    }

    @NotNull
    public Collection<M> add(@NotNull final Collection<M> records) {
        return repository.add(records);
    }

    @NotNull
    public Collection<M> set(@NotNull final Collection<M> records) {
        clear();
        return repository.add(records);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) {
            return findAll();
        }
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws IncorrectIndexException {
        return repository.findOneByIndex(check(index));
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        check(EntityField.ID, id);
        return repository.findOneById(id);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        check(EntityField.ID, id);
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public M remove(@Nullable final M record) throws AbstractException {
        check(Entity.ABSTRACT, record);
        return repository.remove(record);
    }

    @NotNull
    @Override
    public M removeByIndex(@Nullable final Integer index) throws AbstractException {
        return repository.removeByIndex(check(index));
    }

    @NotNull
    @Override
    public M removeById(@Nullable final String id) throws AbstractException {
        check(EntityField.ID, id);
        return repository.removeById(id);
    }

    @Override
    public int totalCount() {
        return repository.totalCount();
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) {
            return findAll();
        }
        return findAll((Comparator<M>) sort.getComparator());
    }

    @NotNull
    @Override
    public Integer check(@Nullable final Integer index) throws IncorrectIndexException {
        if (index == null ||
                index < 0 ||
                index > repository.totalCount()) throw new IncorrectIndexException();
        return index;
    }

}

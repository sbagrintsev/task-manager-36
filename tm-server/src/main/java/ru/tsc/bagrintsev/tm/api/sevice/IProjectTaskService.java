package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Task;

public interface IProjectTaskService extends Checkable {

    Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException;

    Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws AbstractException;

    void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException;

}

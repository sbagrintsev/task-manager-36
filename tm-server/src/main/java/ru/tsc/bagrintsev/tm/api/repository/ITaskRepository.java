package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(
            @NotNull final String userId,
            @NotNull final String name
    );

    @NotNull
    Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    Task setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) throws AbstractException;

}

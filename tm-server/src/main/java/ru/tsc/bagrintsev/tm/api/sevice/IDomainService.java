package ru.tsc.bagrintsev.tm.api.sevice;

public interface IDomainService {

    void loadBackup();

    void saveBackup();

    void loadBase64();

    void saveBase64();

    void loadBinary();

    void saveBinary();

    void loadJacksonJSON();

    void saveJacksonJSON();

    void loadJacksonXML();

    void saveJacksonXML();

    void loadJacksonYAML();

    void saveJacksonYAML();

    void loadJaxbJSON();

    void saveJaxbJSON();

    void loadJaxbXML();

    void saveJaxbXML();

}

package ru.tsc.sbagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.bagrintsev.tm.service.UserService;
import ru.tsc.bagrintsev.tm.util.HashUtil;
import ru.tsc.sbagrintsev.tm.marker.UnitCategory;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

public final class UserServiceTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @Nullable
    private UserService userService;

    @Before
    public void setUp() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
    }

    @Test(expected = IncorrectParameterNameException.class)
    @Category(UnitCategory.class)
    public void testSetParameter() throws AbstractException, GeneralSecurityException {
        @NotNull final User user1 = userService.create("testLogin", "testPassword");
        Assert.assertNull(user1.getEmail());
        userService.setParameter(user1, EntityField.EMAIL, "test@email.ru");
        Assert.assertEquals("test@email.ru", user1.getEmail());
        userService.setParameter(user1, EntityField.NAME, "error");
    }

    @Test
    @Category(UnitCategory.class)
    public void testSetRole() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertEquals(Role.REGULAR, user.getRole());
        userService.setRole("testLogin", Role.ADMIN);
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    @Category(UnitCategory.class)
    public void testSetPassword() throws GeneralSecurityException, AbstractException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        user.setId("id1");
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        @Nullable final String expectedPassHash =  HashUtil.generateHash("testPassword", user.getPasswordSalt(), iterations, keyLength);
        Assert.assertEquals(expectedPassHash, user.getPasswordHash());
        userService.setPassword("id1", "newPassword", "testPassword");
        @Nullable final String expectedNewPassHash =  HashUtil.generateHash("newPassword", user.getPasswordSalt(), iterations, keyLength);
        Assert.assertEquals(expectedNewPassHash, user.getPasswordHash());
    }

    @Test(expected = UserNotFoundException.class)
    @Category(UnitCategory.class)
    public void testFindByLogin() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertEquals(user, userService.findByLogin("login1"));
        Assert.assertEquals(user, userService.findByLogin("wrong"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(UnitCategory.class)
    public void testFindByEmail() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        user.setEmail("test@email.ru");
        Assert.assertEquals(user, userService.findByEmail("test@email.ru"));
        Assert.assertEquals(user, userService.findByEmail("wrong"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveByLogin() throws AbstractException, GeneralSecurityException {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertNotNull(userService.findByLogin("testLogin"));
        Assert.assertEquals(user, userService.removeByLogin("testLogin"));
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void testAdd() throws AbstractException, GeneralSecurityException {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals(user, userService.findAll().get(0));
    }

    @Test
    @Category(UnitCategory.class)
    public void testCreate() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin", "testPass");
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals("testLogin", userService.findAll().get(0).getLogin());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindAll() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin1", "testPass1");
        userService.create("testLogin2", "testPass2");
        userService.create("testLogin3", "testPass3");
        Assert.assertEquals(3, userService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindOneByIndex() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin2", "testPass2");
        userService.create("testLogin1", "testPass1");
        Assert.assertEquals("testLogin1", userService.findOneByIndex(1).getLogin());
        Assert.assertEquals("testLogin2", userService.findOneByIndex(0).getLogin());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindOneById() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        user.setId("id1");
        @NotNull final User user2 = userService.create("testLogin2", "testPassword2");
        user2.setId("id2");
        Assert.assertEquals("testLogin", userService.findOneById("id1").getLogin());
        Assert.assertEquals("testLogin2", userService.findOneById("id2").getLogin());
    }

    @Test(expected = UserNotFoundException.class)
    @Category(UnitCategory.class)
    public void testExistsById() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        user.setId("id1");
        Assert.assertTrue(userService.existsById("id1"));
        Assert.assertTrue(userService.existsById("id2"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemove() throws GeneralSecurityException, AbstractException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertTrue(userService.findAll().contains(user));
        Assert.assertEquals(user, userService.remove(user));
        Assert.assertFalse(userService.findAll().contains(user));
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveByIndex() throws GeneralSecurityException, AbstractException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertTrue(userService.findAll().contains(user));
        Assert.assertEquals(user, userService.removeByIndex(0));
        Assert.assertFalse(userService.findAll().contains(user));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(UnitCategory.class)
    public void testRemoveById() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        user.setId("id1");
        Assert.assertTrue(userService.findAll().contains(user));
        Assert.assertEquals(user, userService.removeById("id1"));
        Assert.assertFalse(userService.findAll().contains(user));
        Assert.assertEquals(user, userService.removeById("id1"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testTotalCount() throws GeneralSecurityException, AbstractException {
        userService.create("testLogin", "testPassword");
        userService.create("testLogin2", "testPassword2");
        Assert.assertEquals(2, userService.totalCount());
    }

    @Test
    @Category(UnitCategory.class)
    public void testClear() throws GeneralSecurityException, AbstractException {
        userService.create("testLogin", "testPassword");
        userService.create("testLogin2", "testPassword2");
        Assert.assertEquals(2, userService.findAll().size());
        userService.clear();
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveAll() {
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User user1 = new User();
        @NotNull final User user2 = new User();
        userList.add(user1);
        userList.add(user2);
        userService.add(userList);
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeAll(userList);
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test(expected = PasswordIsIncorrectException.class)
    @Category(UnitCategory.class)
    public void testCheckUser() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertNotNull(userService.checkUser("testLogin", "testPassword"));
        Assert.assertNotNull(userService.checkUser("testLogin", "wrongPassword"));
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void testCheckUserLocked() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertNotNull(userService.checkUser("testLogin", "testPassword"));
        user.setLocked(true);
        Assert.assertNotNull(userService.checkUser("testLogin", "testPassword"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testIsLoginExists() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertTrue(userService.isLoginExists("testLogin"));
        Assert.assertFalse(userService.isLoginExists("wrongLogin"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testIsEmailExists() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        user.setEmail("test@email.ru");
        Assert.assertTrue(userService.isEmailExists("test@email.ru"));
        Assert.assertFalse(userService.isEmailExists("wrong@email.ru"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testUpdateUser() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        user.setId("qwer");
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        Assert.assertNull(user.getLastName());
        userService.updateUser("qwer", "testFirst", "testLast", "testMiddle");
        Assert.assertEquals("testFirst", user.getFirstName());
        Assert.assertEquals("testLast", user.getLastName());
        Assert.assertEquals("testMiddle", user.getMiddleName());
    }

    @Test
    @Category(UnitCategory.class)
    public void testLockByLogin() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin("testLogin");
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void testUnlockByLogin() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        userService.lockUserByLogin("testLogin");
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin("testLogin");
        Assert.assertFalse(user.getLocked());
    }
    
}

package ru.tsc.sbagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.service.ProjectTaskService;
import ru.tsc.sbagrintsev.tm.marker.UnitCategory;

public final class ProjectTaskServiceTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private ProjectRepository projectRepository;

    @NotNull
    private TaskRepository taskRepository;

    @NotNull
    private ProjectTaskService projectTaskService;

    @NotNull
    private Project project;

    @NotNull
    private Task task;

    @Before
    public void setUp() {
        projectRepository = new ProjectRepository();
        taskRepository = new TaskRepository();
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        project = new Project();
        project.setId("projectId1");
        project.setName("projectName1");
        projectRepository.add(userId, project);
        task = new Task();
        task.setId("taskId1");
        task.setName("taskName1");
        taskRepository.add(userId, task);
    }

    @Test
    @Category(UnitCategory.class)
    public void testBindTaskToProject() throws AbstractException {
        Assert.assertNull(taskRepository.findAll().get(0).getProjectId());
        projectTaskService.bindTaskToProject(userId, "projectId1", "taskId1");
        Assert.assertEquals("projectId1", taskRepository.findOneById("taskId1").getProjectId());
    }

    @Test
    @Category(UnitCategory.class)
    public void testUnbindTaskFromProject() throws AbstractException {
        projectTaskService.bindTaskToProject(userId, "projectId1", "taskId1");
        Assert.assertEquals("projectId1", taskRepository.findOneById("taskId1").getProjectId());
        projectTaskService.unbindTaskFromProject(userId, "taskId1");
        Assert.assertNull(taskRepository.findAll().get(0).getProjectId());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveProjectById() throws AbstractException {
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        projectTaskService.bindTaskToProject(userId, "projectId1", "taskId1");
        projectTaskService.removeProjectById(userId, "projectId1");
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

}

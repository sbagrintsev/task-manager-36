package ru.tsc.sbagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.service.AuthService;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.bagrintsev.tm.service.UserService;
import ru.tsc.sbagrintsev.tm.marker.UnitCategory;

import java.security.GeneralSecurityException;

public final class AuthServiceTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private IAuthService authService;

    @Nullable
    private String token;

    @Before
    public void setUp() throws AbstractException, GeneralSecurityException {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @Nullable IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
        authService = new AuthService(userService, propertyService);
        @Nullable User user = userService.create("testLogin", "testPassword");
        token = null;
    }

    @Test
    @Category(UnitCategory.class)
    public void testSignIn() {
        Assert.assertNull(token);
        token = authService.signIn("testLogin", "testPassword");
        Assert.assertNotNull(token);
    }

    @Test
    @Category(UnitCategory.class)
    public void testValidateToken() {
        token = authService.signIn("testLogin", "testPassword");
        Assert.assertNotNull(token);
        Assert.assertNotNull(authService.validateToken(token));
    }

}

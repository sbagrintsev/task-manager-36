package ru.tsc.sbagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.util.HashUtil;
import ru.tsc.sbagrintsev.tm.marker.UnitCategory;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

public final class UserRepositoryTest {

    @NotNull
    private UserRepository userRepository;

    @Before
    public void setUp() {
        userRepository = new UserRepository();
    }

    @Test(expected = IncorrectParameterNameException.class)
    @Category(UnitCategory.class)
    public void testSetParameter() throws IncorrectParameterNameException {
        @NotNull final User user1 = new User();
        Assert.assertNull(user1.getEmail());
        userRepository.setParameter(user1, EntityField.EMAIL, "test@email.ru");
        Assert.assertEquals("test@email.ru", user1.getEmail());
        userRepository.setParameter(user1, EntityField.NAME, "error");
    }

    @Test
    @Category(UnitCategory.class)
    public void testSetRole() {
        @NotNull final User user = new User();
        Assert.assertEquals(Role.REGULAR, user.getRole());
        userRepository.setRole(user, Role.ADMIN);
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    @Category(UnitCategory.class)
    public void testSetUserPassword() throws GeneralSecurityException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        @NotNull final byte[] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        Assert.assertNull(user.getPasswordHash());
        Assert.assertNull(user.getPasswordSalt());
        userRepository.setUserPassword(user, passwordHash1, salt1);
        Assert.assertEquals(salt1, user.getPasswordSalt());
        Assert.assertEquals(passwordHash1, user.getPasswordHash());
    }

    @Test(expected = UserNotFoundException.class)
    @Category(UnitCategory.class)
    public void testFindByLogin() throws UserNotFoundException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findByLogin("login1"));
        Assert.assertEquals(user, userRepository.findByLogin("wrong"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(UnitCategory.class)
    public void testFindByEmail() throws UserNotFoundException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        user.setEmail("test@email.ru");
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findByEmail("test@email.ru"));
        Assert.assertEquals(user, userRepository.findByEmail("wrong"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveByLogin() throws UserNotFoundException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findByLogin("login1"));
        Assert.assertEquals(user, userRepository.removeByLogin("login1"));
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void testAdd() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull final User user = new User();
        userRepository.add(user);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertEquals(user, userRepository.findAll().get(0));
    }

    @Test
    @Category(UnitCategory.class)
    public void testAddCollection() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User user1 = new User();
        @NotNull final User user2 = new User();
        userList.add(user1);
        userList.add(user2);
        userRepository.add(userList);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertEquals(2, userRepository.totalCount());
        Assert.assertEquals(user2, userRepository.findOneByIndex(1));
    }

    @Test
    @Category(UnitCategory.class)
    public void testCreate() {
        userRepository.create("testLogin", "testPass", "salt".getBytes());
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertEquals("testLogin", userRepository.findAll().get(0).getLogin());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindAll() {
        userRepository.create("testLogin1", "testPass1", "salt1".getBytes());
        userRepository.create("testLogin2", "testPass2", "salt2".getBytes());
        userRepository.create("testLogin3", "testPass3", "salt3".getBytes());
        Assert.assertEquals(3, userRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindOneByIndex() {
        userRepository.create("testLogin2", "testPass2", "salt2".getBytes());
        userRepository.create("testLogin1", "testPass1", "salt1".getBytes());
        Assert.assertEquals("testLogin1", userRepository.findOneByIndex(1).getLogin());
        Assert.assertEquals("testLogin2", userRepository.findOneByIndex(0).getLogin());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindOneById() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        @NotNull final byte[] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        @NotNull final User user2 = new User();
        user2.setId("id2");
        user2.setLogin("login2");
        @NotNull final byte[] salt2 = HashUtil.generateSalt();
        @NotNull final String passwordHash2 = HashUtil.generateHash("pass2", salt2, 50, 10);
        user2.setPasswordHash(passwordHash2);
        user2.setPasswordSalt(salt2);
        userRepository.add(user);
        userRepository.add(user2);
        Assert.assertEquals("login1", userRepository.findOneById("id1").getLogin());
        Assert.assertEquals("login2", userRepository.findOneById("id2").getLogin());
    }

    @Test(expected = UserNotFoundException.class)
    @Category(UnitCategory.class)
    public void testExistsById() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        @NotNull final byte[] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        userRepository.add(user);
        Assert.assertTrue(userRepository.existsById("id1"));
        Assert.assertTrue(userRepository.existsById("id2"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemove() throws GeneralSecurityException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        @NotNull final byte[] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        userRepository.add(user);
        Assert.assertTrue(userRepository.findAll().contains(user));
        Assert.assertEquals(user, userRepository.remove(user));
        Assert.assertFalse(userRepository.findAll().contains(user));
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveByIndex() throws GeneralSecurityException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        @NotNull final byte[] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        userRepository.add(user);
        Assert.assertTrue(userRepository.findAll().contains(user));
        Assert.assertEquals(user, userRepository.removeByIndex(0));
        Assert.assertFalse(userRepository.findAll().contains(user));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(UnitCategory.class)
    public void testRemoveById() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        @NotNull final byte[] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        userRepository.add(user);
        Assert.assertTrue(userRepository.findAll().contains(user));
        Assert.assertEquals(user, userRepository.removeById("id1"));
        Assert.assertFalse(userRepository.findAll().contains(user));
        Assert.assertEquals(user, userRepository.removeById("id1"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testTotalCount() throws GeneralSecurityException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        @NotNull final byte[] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        @NotNull final User user2 = new User();
        user2.setId("id2");
        user2.setLogin("login2");
        @NotNull final byte[] salt2 = HashUtil.generateSalt();
        @NotNull final String passwordHash2 = HashUtil.generateHash("pass2", salt2, 50, 10);
        user2.setPasswordHash(passwordHash2);
        user2.setPasswordSalt(salt2);
        userRepository.add(user);
        userRepository.add(user2);
        Assert.assertEquals(2, userRepository.totalCount());
    }

    @Test
    @Category(UnitCategory.class)
    public void testClear() throws GeneralSecurityException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        @NotNull final byte[] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        @NotNull final User user2 = new User();
        user2.setId("id2");
        user2.setLogin("login2");
        @NotNull final byte[] salt2 = HashUtil.generateSalt();
        @NotNull final String passwordHash2 = HashUtil.generateHash("pass2", salt2, 50, 10);
        user2.setPasswordHash(passwordHash2);
        user2.setPasswordSalt(salt2);
        userRepository.add(user);
        userRepository.add(user2);
        Assert.assertEquals(2, userRepository.findAll().size());
        userRepository.clear();
        Assert.assertEquals(0, userRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveAll() {
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User user1 = new User();
        @NotNull final User user2 = new User();
        userList.add(user1);
        userList.add(user2);
        userRepository.add(userList);
        Assert.assertEquals(2, userRepository.findAll().size());
        userRepository.removeAll(userList);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

}

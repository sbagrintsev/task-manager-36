package ru.tsc.bagrintsev.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private String statusValue;

    public ProjectChangeStatusByIndexRequest(@Nullable String token) {
        super(token);
    }

    public ProjectChangeStatusByIndexRequest(@Nullable String token, @Nullable Integer index, @Nullable String statusValue) {
        super(token);
        this.index = index;
        this.statusValue = statusValue;
    }

}

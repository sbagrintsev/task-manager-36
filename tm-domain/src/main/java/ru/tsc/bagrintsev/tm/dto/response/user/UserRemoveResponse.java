package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserRemoveResponse extends AbstractUserResponse {
}

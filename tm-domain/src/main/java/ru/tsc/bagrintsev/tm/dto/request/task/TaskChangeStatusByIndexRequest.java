package ru.tsc.bagrintsev.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private String statusValue;

    public TaskChangeStatusByIndexRequest(@Nullable String token) {
        super(token);
    }

    public TaskChangeStatusByIndexRequest(@Nullable String token, @Nullable Integer index, @Nullable String statusValue) {
        super(token);
        this.index = index;
        this.statusValue = statusValue;
    }

}

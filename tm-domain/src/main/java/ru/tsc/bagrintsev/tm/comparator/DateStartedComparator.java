package ru.tsc.bagrintsev.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.model.IHasDateStarted;

import java.util.Comparator;

public enum DateStartedComparator implements Comparator<IHasDateStarted> {

    INSTANCE;

    @Override
    public int compare(
            @Nullable final IHasDateStarted o1,
            @Nullable final IHasDateStarted o2
    ) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateStarted() == null || o2.getDateStarted() == null) return 0;
        return o1.getDateStarted().compareTo(o2.getDateStarted());
    }
}

package ru.tsc.bagrintsev.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectUpdateByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectUpdateByIndexRequest(@Nullable String token) {
        super(token);
    }

    public ProjectUpdateByIndexRequest(@Nullable String token, @Nullable Integer index, @Nullable String name, @Nullable String description) {
        super(token);
        this.index = index;
        this.name = name;
        this.description = description;
    }

}

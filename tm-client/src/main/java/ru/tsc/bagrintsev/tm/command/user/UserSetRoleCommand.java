package ru.tsc.bagrintsev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSetRoleRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;

public final class UserSetRoleCommand extends AbstractUserCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo(EntityField.ROLE);
        System.out.println(Arrays.toString(Role.values()));
        @NotNull final Role role = Role.toRole(TerminalUtil.nextLine());
        @NotNull final UserSetRoleRequest request = new UserSetRoleRequest(getToken());
        request.setRole(role);
        request.setLogin(login);
        getUserEndpoint().setRole(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-set-role";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Set role to user.";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

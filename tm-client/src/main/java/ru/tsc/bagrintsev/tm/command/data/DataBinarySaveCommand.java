package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        getDomainEndpoint().saveBinary(new DataBinarySaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-bin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in binary file";
    }

}

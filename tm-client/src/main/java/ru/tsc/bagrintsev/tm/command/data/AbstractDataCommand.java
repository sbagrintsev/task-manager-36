package ru.tsc.bagrintsev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.ServiceLocatorNotInitializedException;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JACKSON_XML = "./data.jackson.xml";

    @NotNull
    public static final String FILE_JACKSON_JSON = "./data.jackson.json";

    @NotNull
    public static final String FILE_JACKSON_YAML = "./data.jackson.yaml";

    @NotNull
    public static final String FILE_JAXB_XML = "./data.jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data.jaxb.json";

    @NotNull
    public IDomainEndpoint getDomainEndpoint() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator.getDomainEndpoint();
    }

    @Override
    public @NotNull String getShortName() {
        return "";
    }

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}

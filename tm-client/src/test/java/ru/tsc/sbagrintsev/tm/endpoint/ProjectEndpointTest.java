package ru.tsc.sbagrintsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.bagrintsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.dto.request.project.*;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.response.project.*;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.sbagrintsev.tm.marker.SoapCategory;

import java.util.List;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @Nullable
    private String tokenTest;

    @NotNull
    private final String WRONG = "wrong";

    @NotNull
    private final String TEST = "test";

    @NotNull
    private final String projectName = "junitProject";

    @NotNull
    private final String projectDescription = "junitDescription";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());

    @Before
    public void setUp() {
        @NotNull final UserSignInResponse responseTest = authEndpoint.signIn(new UserSignInRequest(TEST, TEST));
        tokenTest = responseTest.getToken();
    }

    @After
    public void tearDown(){
        projectEndpoint.clearProject(new ProjectClearRequest(tokenTest));
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription)
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        @Nullable final String id = project.getId();
        @Nullable final Status status = project.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        project = projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(tokenTest, id, "IN_PROGRESS")).getProject();
        Assert.assertNotNull(project);
        Assert.assertNotEquals(Status.NOT_STARTED, project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription)
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        @Nullable final List<Project> projects = projectEndpoint.listProject(new ProjectListRequest(tokenTest, null)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        final int index = projects.size() - 1;
        @Nullable final Status status = project.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        project = projectEndpoint.changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(tokenTest, index, "IN_PROGRESS")).getProject();
        Assert.assertNotNull(project);
        Assert.assertNotEquals(Status.NOT_STARTED, project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void testClearProject() {
        projectEndpoint.createProject(new ProjectCreateRequest(tokenTest, projectName, projectDescription));
        @Nullable List<Project> projects = projectEndpoint.listProject(new ProjectListRequest(tokenTest, null)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        projectEndpoint.clearProject(new ProjectClearRequest(tokenTest));
        projects = projectEndpoint.listProject(new ProjectListRequest(tokenTest, null)).getProjects();
        Assert.assertNull(projects);
    }

    @Test
    public void testCreateProject() {
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest("", "", ""))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(tokenTest, null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(WRONG, null, null))
        );
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription)
        );
        Assert.assertNotNull(response);
        @Nullable final Project project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(projectDescription, project.getDescription());
    }

    @Test
    public void testListProject() {
        projectEndpoint.createProject(new ProjectCreateRequest(tokenTest, projectName, projectDescription));
        @Nullable List<Project> projects = projectEndpoint.listProject(new ProjectListRequest(tokenTest, null)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
        projectEndpoint.createProject(new ProjectCreateRequest(tokenTest, projectName, projectDescription));
        projects = projectEndpoint.listProject(new ProjectListRequest(tokenTest, null)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void testRemoveProjectById() {
        projectEndpoint.createProject(new ProjectCreateRequest(tokenTest, projectName, projectDescription));
        projectEndpoint.createProject(new ProjectCreateRequest(tokenTest, projectName, projectDescription));
        @Nullable List<Project> projects = projectEndpoint.listProject(new ProjectListRequest(tokenTest, null)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        @NotNull final String idFirst = projects.get(0).getId();
        @NotNull final ProjectRemoveByIdResponse response =  projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(tokenTest, idFirst));
        Assert.assertNotNull(response);
        projects = projectEndpoint.listProject(new ProjectListRequest(tokenTest, null)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void testRemoveProjectByIndex() {
        projectEndpoint.createProject(new ProjectCreateRequest(tokenTest, projectName, projectDescription));
        projectEndpoint.createProject(new ProjectCreateRequest(tokenTest, projectName, projectDescription));
        @Nullable List<Project> projects = projectEndpoint.listProject(new ProjectListRequest(tokenTest, null)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        @NotNull final ProjectRemoveByIndexResponse response = projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(tokenTest, 0));
        Assert.assertNotNull(response);
        projects = projectEndpoint.listProject(new ProjectListRequest(tokenTest, null)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void testShowProjectById() {
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription)
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        @NotNull String id = project.getId();
        @NotNull final ProjectShowByIdResponse projectShowByIdResponse = projectEndpoint.showProjectById(new ProjectShowByIdRequest(tokenTest, id));
        Assert.assertNotNull(projectShowByIdResponse);
        Assert.assertEquals(projectName, projectShowByIdResponse.getProject().getName());
    }

    @Test
    public void testShowProjectByIndex() {
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription)
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        @NotNull final ProjectShowByIndexResponse projectShowByIndexResponse = projectEndpoint.showProjectByIndex(new ProjectShowByIndexRequest(tokenTest, 0));
        Assert.assertNotNull(projectShowByIndexResponse);
        Assert.assertEquals(projectName, projectShowByIndexResponse.getProject().getName());
    }

    @Test
    public void testUpdateProjectById() {
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription)
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        @Nullable String id = project.getId();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
        @NotNull final ProjectUpdateByIdResponse projectUpdateByIdResponse = projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(tokenTest, id, "newName", "newDescription"));
        Assert.assertNotNull(projectUpdateByIdResponse);
        Assert.assertEquals(id, projectUpdateByIdResponse.getProject().getId());
        Assert.assertEquals("newName", projectUpdateByIdResponse.getProject().getName());
        Assert.assertEquals("newDescription", projectUpdateByIdResponse.getProject().getDescription());
    }

    @Test
    public void testUpdateProjectByIndex() {
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription)
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        @Nullable String id = project.getId();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
        @NotNull final ProjectUpdateByIndexResponse projectUpdateByIndexResponse = projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(tokenTest, 0, "newName", "newDescription"));
        Assert.assertNotNull(projectUpdateByIndexResponse);
        Assert.assertEquals(id, projectUpdateByIndexResponse.getProject().getId());
        Assert.assertEquals("newName", projectUpdateByIndexResponse.getProject().getName());
        Assert.assertEquals("newDescription", projectUpdateByIndexResponse.getProject().getDescription());
    }

}
